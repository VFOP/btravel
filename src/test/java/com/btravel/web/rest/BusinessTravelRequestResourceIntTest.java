package com.btravel.web.rest;

import com.btravel.Application;
import com.btravel.domain.BusinessTravelRequest;
import com.btravel.repository.BusinessTravelRequestRepository;
import com.btravel.service.BusinessTravelRequestService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.btravel.domain.enumeration.StatusBTR;

/**
 * Test class for the BusinessTravelRequestResource REST controller.
 *
 * @see BusinessTravelRequestResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class BusinessTravelRequestResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.of("Z"));

    
    private static final StatusBTR DEFAULT_STATUS = StatusBTR.INITIATED;
    private static final StatusBTR UPDATED_STATUS = StatusBTR.WAITING_FOR_APPROVAL;

    private static final ZonedDateTime DEFAULT_START_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_START_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_START_DATE_STR = dateTimeFormatter.format(DEFAULT_START_DATE);

    private static final ZonedDateTime DEFAULT_END_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_END_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_END_DATE_STR = dateTimeFormatter.format(DEFAULT_END_DATE);
    private static final String DEFAULT_LOCATION = "AAAAA";
    private static final String UPDATED_LOCATION = "BBBBB";
    private static final String DEFAULT_CENTER_COST = "AAAAA";
    private static final String UPDATED_CENTER_COST = "BBBBB";

    private static final ZonedDateTime DEFAULT_REQUEST_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_REQUEST_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_REQUEST_DATE_STR = dateTimeFormatter.format(DEFAULT_REQUEST_DATE);

    private static final ZonedDateTime DEFAULT_LAST_MODIFIED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_LAST_MODIFIED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_LAST_MODIFIED_DATE_STR = dateTimeFormatter.format(DEFAULT_LAST_MODIFIED_DATE);

    private static final Double DEFAULT_TOTAL_COST = 1D;
    private static final Double UPDATED_TOTAL_COST = 2D;

    @Inject
    private BusinessTravelRequestRepository businessTravelRequestRepository;

    @Inject
    private BusinessTravelRequestService businessTravelRequestService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restBusinessTravelRequestMockMvc;

    private BusinessTravelRequest businessTravelRequest;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        BusinessTravelRequestResource businessTravelRequestResource = new BusinessTravelRequestResource();
        ReflectionTestUtils.setField(businessTravelRequestResource, "businessTravelRequestService", businessTravelRequestService);
        this.restBusinessTravelRequestMockMvc = MockMvcBuilders.standaloneSetup(businessTravelRequestResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        businessTravelRequest = new BusinessTravelRequest();
        businessTravelRequest.setStatus(DEFAULT_STATUS);
        businessTravelRequest.setStartDate(DEFAULT_START_DATE);
        businessTravelRequest.setEndDate(DEFAULT_END_DATE);
        businessTravelRequest.setLocation(DEFAULT_LOCATION);
        businessTravelRequest.setCenterCost(DEFAULT_CENTER_COST);
        businessTravelRequest.setRequestDate(DEFAULT_REQUEST_DATE);
        businessTravelRequest.setLastModifiedDate(DEFAULT_LAST_MODIFIED_DATE);
        businessTravelRequest.setTotalCost(DEFAULT_TOTAL_COST);
    }

    @Test
    @Transactional
    public void createBusinessTravelRequest() throws Exception {
        int databaseSizeBeforeCreate = businessTravelRequestRepository.findAll().size();

        // Create the BusinessTravelRequest

        restBusinessTravelRequestMockMvc.perform(post("/api/businessTravelRequests")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(businessTravelRequest)))
                .andExpect(status().isCreated());

        // Validate the BusinessTravelRequest in the database
        List<BusinessTravelRequest> businessTravelRequests = businessTravelRequestRepository.findAll();
        assertThat(businessTravelRequests).hasSize(databaseSizeBeforeCreate + 1);
        BusinessTravelRequest testBusinessTravelRequest = businessTravelRequests.get(businessTravelRequests.size() - 1);
        assertThat(testBusinessTravelRequest.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testBusinessTravelRequest.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testBusinessTravelRequest.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testBusinessTravelRequest.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testBusinessTravelRequest.getCenterCost()).isEqualTo(DEFAULT_CENTER_COST);
        assertThat(testBusinessTravelRequest.getRequestDate()).isEqualTo(DEFAULT_REQUEST_DATE);
        assertThat(testBusinessTravelRequest.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testBusinessTravelRequest.getTotalCost()).isEqualTo(DEFAULT_TOTAL_COST);
    }

    @Test
    @Transactional
    public void getAllBusinessTravelRequests() throws Exception {
        // Initialize the database
        businessTravelRequestRepository.saveAndFlush(businessTravelRequest);

        // Get all the businessTravelRequests
        restBusinessTravelRequestMockMvc.perform(get("/api/businessTravelRequests?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(businessTravelRequest.getId().intValue())))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
                .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE_STR)))
                .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE_STR)))
                .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION.toString())))
                .andExpect(jsonPath("$.[*].centerCost").value(hasItem(DEFAULT_CENTER_COST.toString())))
                .andExpect(jsonPath("$.[*].requestDate").value(hasItem(DEFAULT_REQUEST_DATE_STR)))
                .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE_STR)))
                .andExpect(jsonPath("$.[*].totalCost").value(hasItem(DEFAULT_TOTAL_COST.doubleValue())));
    }

    @Test
    @Transactional
    public void getBusinessTravelRequest() throws Exception {
        // Initialize the database
        businessTravelRequestRepository.saveAndFlush(businessTravelRequest);

        // Get the businessTravelRequest
        restBusinessTravelRequestMockMvc.perform(get("/api/businessTravelRequests/{id}", businessTravelRequest.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(businessTravelRequest.getId().intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE_STR))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE_STR))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION.toString()))
            .andExpect(jsonPath("$.centerCost").value(DEFAULT_CENTER_COST.toString()))
            .andExpect(jsonPath("$.requestDate").value(DEFAULT_REQUEST_DATE_STR))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE_STR))
            .andExpect(jsonPath("$.totalCost").value(DEFAULT_TOTAL_COST.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingBusinessTravelRequest() throws Exception {
        // Get the businessTravelRequest
        restBusinessTravelRequestMockMvc.perform(get("/api/businessTravelRequests/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBusinessTravelRequest() throws Exception {
        // Initialize the database
        businessTravelRequestRepository.saveAndFlush(businessTravelRequest);

		int databaseSizeBeforeUpdate = businessTravelRequestRepository.findAll().size();

        // Update the businessTravelRequest
        businessTravelRequest.setStatus(UPDATED_STATUS);
        businessTravelRequest.setStartDate(UPDATED_START_DATE);
        businessTravelRequest.setEndDate(UPDATED_END_DATE);
        businessTravelRequest.setLocation(UPDATED_LOCATION);
        businessTravelRequest.setCenterCost(UPDATED_CENTER_COST);
        businessTravelRequest.setRequestDate(UPDATED_REQUEST_DATE);
        businessTravelRequest.setLastModifiedDate(UPDATED_LAST_MODIFIED_DATE);
        businessTravelRequest.setTotalCost(UPDATED_TOTAL_COST);

        restBusinessTravelRequestMockMvc.perform(put("/api/businessTravelRequests")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(businessTravelRequest)))
                .andExpect(status().isOk());

        // Validate the BusinessTravelRequest in the database
        List<BusinessTravelRequest> businessTravelRequests = businessTravelRequestRepository.findAll();
        assertThat(businessTravelRequests).hasSize(databaseSizeBeforeUpdate);
        BusinessTravelRequest testBusinessTravelRequest = businessTravelRequests.get(businessTravelRequests.size() - 1);
        assertThat(testBusinessTravelRequest.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testBusinessTravelRequest.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testBusinessTravelRequest.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testBusinessTravelRequest.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testBusinessTravelRequest.getCenterCost()).isEqualTo(UPDATED_CENTER_COST);
        assertThat(testBusinessTravelRequest.getRequestDate()).isEqualTo(UPDATED_REQUEST_DATE);
        assertThat(testBusinessTravelRequest.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testBusinessTravelRequest.getTotalCost()).isEqualTo(UPDATED_TOTAL_COST);
    }

    @Test
    @Transactional
    public void deleteBusinessTravelRequest() throws Exception {
        // Initialize the database
        businessTravelRequestRepository.saveAndFlush(businessTravelRequest);

		int databaseSizeBeforeDelete = businessTravelRequestRepository.findAll().size();

        // Get the businessTravelRequest
        restBusinessTravelRequestMockMvc.perform(delete("/api/businessTravelRequests/{id}", businessTravelRequest.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<BusinessTravelRequest> businessTravelRequests = businessTravelRequestRepository.findAll();
        assertThat(businessTravelRequests).hasSize(databaseSizeBeforeDelete - 1);
    }
}
