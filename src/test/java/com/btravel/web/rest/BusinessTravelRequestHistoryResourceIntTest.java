package com.btravel.web.rest;

import com.btravel.Application;
import com.btravel.domain.BusinessTravelRequestHistory;
import com.btravel.repository.BusinessTravelRequestHistoryRepository;
import com.btravel.service.BusinessTravelRequestHistoryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.btravel.domain.enumeration.StatusBTR;

/**
 * Test class for the BusinessTravelRequestHistoryResource REST controller.
 *
 * @see BusinessTravelRequestHistoryResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class BusinessTravelRequestHistoryResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.of("Z"));


    private static final StatusBTR DEFAULT_STATUS = StatusBTR.INITIATED;
    private static final StatusBTR UPDATED_STATUS = StatusBTR.WAITING_FOR_APPROVAL;

    private static final ZonedDateTime DEFAULT_START_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_START_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_START_DATE_STR = dateTimeFormatter.format(DEFAULT_START_DATE);

    private static final ZonedDateTime DEFAULT_END_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_END_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_END_DATE_STR = dateTimeFormatter.format(DEFAULT_END_DATE);
    private static final String DEFAULT_LOCATION = "AAAAA";
    private static final String UPDATED_LOCATION = "BBBBB";
    private static final String DEFAULT_CENTER_COST = "AAAAA";
    private static final String UPDATED_CENTER_COST = "BBBBB";

    private static final ZonedDateTime DEFAULT_REQUEST_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_REQUEST_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_REQUEST_DATE_STR = dateTimeFormatter.format(DEFAULT_REQUEST_DATE);

    private static final ZonedDateTime DEFAULT_LAST_MODIFIED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_LAST_MODIFIED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_LAST_MODIFIED_DATE_STR = dateTimeFormatter.format(DEFAULT_LAST_MODIFIED_DATE);

    private static final Double DEFAULT_TOTAL_COST = 1D;
    private static final Double UPDATED_TOTAL_COST = 2D;
    private static final String DEFAULT_EMPLOYEE = "AAAAA";
    private static final String UPDATED_EMPLOYEE = "BBBBB";
    private static final String DEFAULT_ASSIGN_TO = "AAAAA";
    private static final String UPDATED_ASSIGN_TO = "BBBBB";
    private static final String DEFAULT_SUPPLIER = "AAAAA";
    private static final String UPDATED_SUPPLIER = "BBBBB";
    private static final String DEFAULT_ASSIGN_FROM = "AAAAA";
    private static final String UPDATED_ASSIGN_FROM = "BBBBB";
    private static final String DEFAULT_MANAGER = "AAAAA";
    private static final String UPDATED_MANAGER = "BBBBB";

    @Inject
    private BusinessTravelRequestHistoryRepository businessTravelRequestHistoryRepository;

    @Inject
    private BusinessTravelRequestHistoryService businessTravelRequestHistoryService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restBusinessTravelRequestHistoryMockMvc;

    private BusinessTravelRequestHistory businessTravelRequestHistory;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        BusinessTravelRequestHistoryResource businessTravelRequestHistoryResource = new BusinessTravelRequestHistoryResource();
        ReflectionTestUtils.setField(businessTravelRequestHistoryResource, "businessTravelRequestHistoryService", businessTravelRequestHistoryService);
        this.restBusinessTravelRequestHistoryMockMvc = MockMvcBuilders.standaloneSetup(businessTravelRequestHistoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        businessTravelRequestHistory = new BusinessTravelRequestHistory();
        businessTravelRequestHistory.setStatus(DEFAULT_STATUS);
        businessTravelRequestHistory.setStartDate(DEFAULT_START_DATE);
        businessTravelRequestHistory.setEndDate(DEFAULT_END_DATE);
        businessTravelRequestHistory.setLocation(DEFAULT_LOCATION);
        businessTravelRequestHistory.setCenterCost(DEFAULT_CENTER_COST);
        businessTravelRequestHistory.setRequestDate(DEFAULT_REQUEST_DATE);
        businessTravelRequestHistory.setLastModifiedDate(DEFAULT_LAST_MODIFIED_DATE);
        businessTravelRequestHistory.setTotalCost(DEFAULT_TOTAL_COST);
        businessTravelRequestHistory.setEmployee(DEFAULT_EMPLOYEE);
        businessTravelRequestHistory.setAssignTo(DEFAULT_ASSIGN_TO);
        businessTravelRequestHistory.setSupplier(DEFAULT_SUPPLIER);
        businessTravelRequestHistory.setAssignFrom(DEFAULT_ASSIGN_FROM);
        businessTravelRequestHistory.setManager(DEFAULT_MANAGER);
    }

    @Test
    @Transactional
    public void createBusinessTravelRequestHistory() throws Exception {
        int databaseSizeBeforeCreate = businessTravelRequestHistoryRepository.findAll().size();

        // Create the BusinessTravelRequestHistory

        restBusinessTravelRequestHistoryMockMvc.perform(post("/api/businessTravelRequestHistorys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(businessTravelRequestHistory)))
                .andExpect(status().isCreated());

        // Validate the BusinessTravelRequestHistory in the database
        List<BusinessTravelRequestHistory> businessTravelRequestHistorys = businessTravelRequestHistoryRepository.findAll();
        assertThat(businessTravelRequestHistorys).hasSize(databaseSizeBeforeCreate + 1);
        BusinessTravelRequestHistory testBusinessTravelRequestHistory = businessTravelRequestHistorys.get(businessTravelRequestHistorys.size() - 1);
        assertThat(testBusinessTravelRequestHistory.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testBusinessTravelRequestHistory.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testBusinessTravelRequestHistory.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testBusinessTravelRequestHistory.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testBusinessTravelRequestHistory.getCenterCost()).isEqualTo(DEFAULT_CENTER_COST);
        assertThat(testBusinessTravelRequestHistory.getRequestDate()).isEqualTo(DEFAULT_REQUEST_DATE);
        assertThat(testBusinessTravelRequestHistory.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testBusinessTravelRequestHistory.getTotalCost()).isEqualTo(DEFAULT_TOTAL_COST);
        assertThat(testBusinessTravelRequestHistory.getEmployee()).isEqualTo(DEFAULT_EMPLOYEE);
        assertThat(testBusinessTravelRequestHistory.getAssignTo()).isEqualTo(DEFAULT_ASSIGN_TO);
        assertThat(testBusinessTravelRequestHistory.getSupplier()).isEqualTo(DEFAULT_SUPPLIER);
        assertThat(testBusinessTravelRequestHistory.getAssignFrom()).isEqualTo(DEFAULT_ASSIGN_FROM);
        assertThat(testBusinessTravelRequestHistory.getManager()).isEqualTo(DEFAULT_MANAGER);
    }

    @Test
    @Transactional
    public void getAllBusinessTravelRequestHistorys() throws Exception {
        // Initialize the database
        businessTravelRequestHistoryRepository.saveAndFlush(businessTravelRequestHistory);

        // Get all the businessTravelRequestHistorys
        restBusinessTravelRequestHistoryMockMvc.perform(get("/api/businessTravelRequestHistorys?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(businessTravelRequestHistory.getId().intValue())))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
                .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE_STR)))
                .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE_STR)))
                .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION.toString())))
                .andExpect(jsonPath("$.[*].centerCost").value(hasItem(DEFAULT_CENTER_COST.toString())))
                .andExpect(jsonPath("$.[*].requestDate").value(hasItem(DEFAULT_REQUEST_DATE_STR)))
                .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE_STR)))
                .andExpect(jsonPath("$.[*].totalCost").value(hasItem(DEFAULT_TOTAL_COST.doubleValue())))
                .andExpect(jsonPath("$.[*].employee").value(hasItem(DEFAULT_EMPLOYEE.toString())))
                .andExpect(jsonPath("$.[*].assignTo").value(hasItem(DEFAULT_ASSIGN_TO.toString())))
                .andExpect(jsonPath("$.[*].supplier").value(hasItem(DEFAULT_SUPPLIER.toString())))
                .andExpect(jsonPath("$.[*].assignFrom").value(hasItem(DEFAULT_ASSIGN_FROM.toString())))
                .andExpect(jsonPath("$.[*].manager").value(hasItem(DEFAULT_MANAGER.toString())));
    }

    @Test
    @Transactional
    public void getBusinessTravelRequestHistory() throws Exception {
        // Initialize the database
        businessTravelRequestHistoryRepository.saveAndFlush(businessTravelRequestHistory);

        // Get the businessTravelRequestHistory
        restBusinessTravelRequestHistoryMockMvc.perform(get("/api/businessTravelRequestHistorys/{id}", businessTravelRequestHistory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(businessTravelRequestHistory.getId().intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE_STR))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE_STR))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION.toString()))
            .andExpect(jsonPath("$.centerCost").value(DEFAULT_CENTER_COST.toString()))
            .andExpect(jsonPath("$.requestDate").value(DEFAULT_REQUEST_DATE_STR))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE_STR))
            .andExpect(jsonPath("$.totalCost").value(DEFAULT_TOTAL_COST.doubleValue()))
            .andExpect(jsonPath("$.employee").value(DEFAULT_EMPLOYEE.toString()))
            .andExpect(jsonPath("$.assignTo").value(DEFAULT_ASSIGN_TO.toString()))
            .andExpect(jsonPath("$.supplier").value(DEFAULT_SUPPLIER.toString()))
            .andExpect(jsonPath("$.assignFrom").value(DEFAULT_ASSIGN_FROM.toString()))
            .andExpect(jsonPath("$.manager").value(DEFAULT_MANAGER.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingBusinessTravelRequestHistory() throws Exception {
        // Get the businessTravelRequestHistory
        restBusinessTravelRequestHistoryMockMvc.perform(get("/api/businessTravelRequestHistorys/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBusinessTravelRequestHistory() throws Exception {
        // Initialize the database
        businessTravelRequestHistoryRepository.saveAndFlush(businessTravelRequestHistory);

		int databaseSizeBeforeUpdate = businessTravelRequestHistoryRepository.findAll().size();

        // Update the businessTravelRequestHistory
        businessTravelRequestHistory.setStatus(UPDATED_STATUS);
        businessTravelRequestHistory.setStartDate(UPDATED_START_DATE);
        businessTravelRequestHistory.setEndDate(UPDATED_END_DATE);
        businessTravelRequestHistory.setLocation(UPDATED_LOCATION);
        businessTravelRequestHistory.setCenterCost(UPDATED_CENTER_COST);
        businessTravelRequestHistory.setRequestDate(UPDATED_REQUEST_DATE);
        businessTravelRequestHistory.setLastModifiedDate(UPDATED_LAST_MODIFIED_DATE);
        businessTravelRequestHistory.setTotalCost(UPDATED_TOTAL_COST);
        businessTravelRequestHistory.setEmployee(UPDATED_EMPLOYEE);
        businessTravelRequestHistory.setAssignTo(UPDATED_ASSIGN_TO);
        businessTravelRequestHistory.setSupplier(UPDATED_SUPPLIER);
        businessTravelRequestHistory.setAssignFrom(UPDATED_ASSIGN_FROM);
        businessTravelRequestHistory.setManager(UPDATED_MANAGER);

        restBusinessTravelRequestHistoryMockMvc.perform(put("/api/businessTravelRequestHistorys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(businessTravelRequestHistory)))
                .andExpect(status().isOk());

        // Validate the BusinessTravelRequestHistory in the database
        List<BusinessTravelRequestHistory> businessTravelRequestHistorys = businessTravelRequestHistoryRepository.findAll();
        assertThat(businessTravelRequestHistorys).hasSize(databaseSizeBeforeUpdate);
        BusinessTravelRequestHistory testBusinessTravelRequestHistory = businessTravelRequestHistorys.get(businessTravelRequestHistorys.size() - 1);
        assertThat(testBusinessTravelRequestHistory.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testBusinessTravelRequestHistory.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testBusinessTravelRequestHistory.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testBusinessTravelRequestHistory.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testBusinessTravelRequestHistory.getCenterCost()).isEqualTo(UPDATED_CENTER_COST);
        assertThat(testBusinessTravelRequestHistory.getRequestDate()).isEqualTo(UPDATED_REQUEST_DATE);
        assertThat(testBusinessTravelRequestHistory.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testBusinessTravelRequestHistory.getTotalCost()).isEqualTo(UPDATED_TOTAL_COST);
        assertThat(testBusinessTravelRequestHistory.getEmployee()).isEqualTo(UPDATED_EMPLOYEE);
        assertThat(testBusinessTravelRequestHistory.getAssignTo()).isEqualTo(UPDATED_ASSIGN_TO);
        assertThat(testBusinessTravelRequestHistory.getSupplier()).isEqualTo(UPDATED_SUPPLIER);
        assertThat(testBusinessTravelRequestHistory.getAssignFrom()).isEqualTo(UPDATED_ASSIGN_FROM);
        assertThat(testBusinessTravelRequestHistory.getManager()).isEqualTo(UPDATED_MANAGER);
    }

    @Test
    @Transactional
    public void deleteBusinessTravelRequestHistory() throws Exception {
        // Initialize the database
        businessTravelRequestHistoryRepository.saveAndFlush(businessTravelRequestHistory);

		int databaseSizeBeforeDelete = businessTravelRequestHistoryRepository.findAll().size();

        // Get the businessTravelRequestHistory
        restBusinessTravelRequestHistoryMockMvc.perform(delete("/api/businessTravelRequestHistorys/{id}", businessTravelRequestHistory.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<BusinessTravelRequestHistory> businessTravelRequestHistorys = businessTravelRequestHistoryRepository.findAll();
        assertThat(businessTravelRequestHistorys).hasSize(databaseSizeBeforeDelete - 1);
    }
}
