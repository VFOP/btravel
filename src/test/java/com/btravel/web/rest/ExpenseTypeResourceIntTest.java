package com.btravel.web.rest;

import com.btravel.Application;
import com.btravel.domain.ExpenseType;
import com.btravel.repository.ExpenseTypeRepository;
import com.btravel.service.ExpenseTypeService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ExpenseTypeResource REST controller.
 *
 * @see ExpenseTypeResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class ExpenseTypeResourceIntTest {

    private static final String DEFAULT_TYPE = "AAAAA";
    private static final String UPDATED_TYPE = "BBBBB";

    @Inject
    private ExpenseTypeRepository expenseTypeRepository;

    @Inject
    private ExpenseTypeService expenseTypeService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restExpenseTypeMockMvc;

    private ExpenseType expenseType;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ExpenseTypeResource expenseTypeResource = new ExpenseTypeResource();
        ReflectionTestUtils.setField(expenseTypeResource, "expenseTypeService", expenseTypeService);
        this.restExpenseTypeMockMvc = MockMvcBuilders.standaloneSetup(expenseTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        expenseType = new ExpenseType();
        expenseType.setType(DEFAULT_TYPE);
    }

    @Test
    @Transactional
    public void createExpenseType() throws Exception {
        int databaseSizeBeforeCreate = expenseTypeRepository.findAll().size();

        // Create the ExpenseType

        restExpenseTypeMockMvc.perform(post("/api/expenseTypes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(expenseType)))
                .andExpect(status().isCreated());

        // Validate the ExpenseType in the database
        List<ExpenseType> expenseTypes = expenseTypeRepository.findAll();
        assertThat(expenseTypes).hasSize(databaseSizeBeforeCreate + 1);
        ExpenseType testExpenseType = expenseTypes.get(expenseTypes.size() - 1);
        assertThat(testExpenseType.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    @Transactional
    public void getAllExpenseTypes() throws Exception {
        // Initialize the database
        expenseTypeRepository.saveAndFlush(expenseType);

        // Get all the expenseTypes
        restExpenseTypeMockMvc.perform(get("/api/expenseTypes?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(expenseType.getId().intValue())))
                .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getExpenseType() throws Exception {
        // Initialize the database
        expenseTypeRepository.saveAndFlush(expenseType);

        // Get the expenseType
        restExpenseTypeMockMvc.perform(get("/api/expenseTypes/{id}", expenseType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(expenseType.getId().intValue()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingExpenseType() throws Exception {
        // Get the expenseType
        restExpenseTypeMockMvc.perform(get("/api/expenseTypes/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateExpenseType() throws Exception {
        // Initialize the database
        expenseTypeRepository.saveAndFlush(expenseType);

		int databaseSizeBeforeUpdate = expenseTypeRepository.findAll().size();

        // Update the expenseType
        expenseType.setType(UPDATED_TYPE);

        restExpenseTypeMockMvc.perform(put("/api/expenseTypes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(expenseType)))
                .andExpect(status().isOk());

        // Validate the ExpenseType in the database
        List<ExpenseType> expenseTypes = expenseTypeRepository.findAll();
        assertThat(expenseTypes).hasSize(databaseSizeBeforeUpdate);
        ExpenseType testExpenseType = expenseTypes.get(expenseTypes.size() - 1);
        assertThat(testExpenseType.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void deleteExpenseType() throws Exception {
        // Initialize the database
        expenseTypeRepository.saveAndFlush(expenseType);

		int databaseSizeBeforeDelete = expenseTypeRepository.findAll().size();

        // Get the expenseType
        restExpenseTypeMockMvc.perform(delete("/api/expenseTypes/{id}", expenseType.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ExpenseType> expenseTypes = expenseTypeRepository.findAll();
        assertThat(expenseTypes).hasSize(databaseSizeBeforeDelete - 1);
    }
}
