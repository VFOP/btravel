'use strict';

describe('Controller Tests', function() {

    describe('Expense Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockExpense, MockBusinessTravelRequest, MockExpenseType;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockExpense = jasmine.createSpy('MockExpense');
            MockBusinessTravelRequest = jasmine.createSpy('MockBusinessTravelRequest');
            MockExpenseType = jasmine.createSpy('MockExpenseType');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Expense': MockExpense,
                'BusinessTravelRequest': MockBusinessTravelRequest,
                'ExpenseType': MockExpenseType
            };
            createController = function() {
                $injector.get('$controller')("ExpenseDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'btravelApp:expenseUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
