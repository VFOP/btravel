'use strict';

describe('Controller Tests', function() {

    describe('BusinessTravelRequestHistory Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockBusinessTravelRequestHistory, MockBusinessTravelRequest;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockBusinessTravelRequestHistory = jasmine.createSpy('MockBusinessTravelRequestHistory');
            MockBusinessTravelRequest = jasmine.createSpy('MockBusinessTravelRequest');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'BusinessTravelRequestHistory': MockBusinessTravelRequestHistory,
                'BusinessTravelRequest': MockBusinessTravelRequest
            };
            createController = function() {
                $injector.get('$controller')("BusinessTravelRequestHistoryDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'btravelApp:businessTravelRequestHistoryUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
