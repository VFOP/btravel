'use strict';

describe('Controller Tests', function() {

    describe('ExpenseType Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockExpenseType, MockExpense;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockExpenseType = jasmine.createSpy('MockExpenseType');
            MockExpense = jasmine.createSpy('MockExpense');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'ExpenseType': MockExpenseType,
                'Expense': MockExpense
            };
            createController = function() {
                $injector.get('$controller')("ExpenseTypeDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'btravelApp:expenseTypeUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
