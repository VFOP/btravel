'use strict';

describe('Controller Tests', function() {

    describe('BusinessTravelRequest Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockBusinessTravelRequest, MockUser, MockExpense, MockComment;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockBusinessTravelRequest = jasmine.createSpy('MockBusinessTravelRequest');
            MockUser = jasmine.createSpy('MockUser');
            MockExpense = jasmine.createSpy('MockExpense');
            MockComment = jasmine.createSpy('MockComment');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'BusinessTravelRequest': MockBusinessTravelRequest,
                'User': MockUser,
                'Expense': MockExpense,
                'Comment': MockComment
            };
            createController = function() {
                $injector.get('$controller')("BusinessTravelRequestDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'btravelApp:businessTravelRequestUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
