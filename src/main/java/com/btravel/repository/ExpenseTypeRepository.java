package com.btravel.repository;

import com.btravel.domain.ExpenseType;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ExpenseType entity.
 */
public interface ExpenseTypeRepository extends JpaRepository<ExpenseType,Long> {

}
