package com.btravel.repository;

import com.btravel.domain.BusinessTravelRequest;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the BusinessTravelRequest entity.
 */
public interface BusinessTravelRequestRepository extends JpaRepository<BusinessTravelRequest,Long> {

    @Query("select businessTravelRequest from BusinessTravelRequest businessTravelRequest where businessTravelRequest.user.login = ?#{principal.username}")
    List<BusinessTravelRequest> findByUserIsCurrentUser();

    @Query("select businessTravelRequest from BusinessTravelRequest businessTravelRequest where businessTravelRequest.assignTo.login = ?#{principal.username}")
    List<BusinessTravelRequest> findByAssignToIsCurrentUser();

    @Query("select businessTravelRequest from BusinessTravelRequest businessTravelRequest where businessTravelRequest.supplier.login = ?#{principal.username}")
    List<BusinessTravelRequest> findBySupplierIsCurrentUser();

    @Query("select businessTravelRequest from BusinessTravelRequest businessTravelRequest where businessTravelRequest.assignFrom.login = ?#{principal.username}")
    List<BusinessTravelRequest> findByAssignFromIsCurrentUser();

    @Query("select businessTravelRequest from BusinessTravelRequest businessTravelRequest where businessTravelRequest.manager.login = ?#{principal.username}")
    List<BusinessTravelRequest> findByManagerIsCurrentUser();

    List<BusinessTravelRequest> findAll();

}
