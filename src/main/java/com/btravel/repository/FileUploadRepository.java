package com.btravel.repository;

import com.btravel.domain.BusinessTravelRequest;
import com.btravel.domain.FileUpload;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by FLORINOPREA93 on 4/14/2016.
 */
public interface FileUploadRepository extends JpaRepository<FileUpload, Long> {

    FileUpload findByFileName(String filename);
    List<FileUpload> findAllByBusinessTravelRequest(BusinessTravelRequest businessTravelRequest);

}
