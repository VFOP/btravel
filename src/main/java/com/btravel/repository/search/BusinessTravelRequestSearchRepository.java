package com.btravel.repository.search;

import com.btravel.domain.BusinessTravelRequest;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the BusinessTravelRequest entity.
 */
public interface BusinessTravelRequestSearchRepository extends ElasticsearchRepository<BusinessTravelRequest, Long> {
}
