package com.btravel.repository.search;

import com.btravel.domain.BusinessTravelRequestHistory;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the BusinessTravelRequestHistory entity.
 */
public interface BusinessTravelRequestHistorySearchRepository extends ElasticsearchRepository<BusinessTravelRequestHistory, Long> {
}
