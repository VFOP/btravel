package com.btravel.repository.search;

import com.btravel.domain.ExpenseType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the ExpenseType entity.
 */
public interface ExpenseTypeSearchRepository extends ElasticsearchRepository<ExpenseType, Long> {
}
