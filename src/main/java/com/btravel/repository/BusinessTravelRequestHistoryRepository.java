package com.btravel.repository;

import com.btravel.domain.BusinessTravelRequestHistory;

import org.springframework.data.jpa.repository.*;

/**
 * Spring Data JPA repository for the BusinessTravelRequestHistory entity.
 */
public interface BusinessTravelRequestHistoryRepository extends JpaRepository<BusinessTravelRequestHistory,Long> {

}
