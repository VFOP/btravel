package com.btravel.repository;

import com.btravel.domain.BusinessTravelRequest;
import com.btravel.domain.Expense;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Expense entity.
 */
public interface ExpenseRepository extends JpaRepository<Expense,Long> {

    List<Expense> findAllByBusinessTravelRequest(BusinessTravelRequest businessTravelRequest);

}
