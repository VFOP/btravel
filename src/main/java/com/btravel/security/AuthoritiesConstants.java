package com.btravel.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_EMPLOYEE";

    public static final String MANAGER = "ROLE_MANAGER";

    public static final String SUPPLIER = "ROLE_SUPPLIER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    private AuthoritiesConstants() {
    }
}
