package com.btravel.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Expense.
 */
@Entity
@Table(name = "expense")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "expense")
public class Expense implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "expense_cost")
    private Double expenseCost;
    
    @ManyToOne
    @JoinColumn(name = "business_travel_request_id")
    private BusinessTravelRequest businessTravelRequest;

    @ManyToOne
    @JoinColumn(name = "expense_type_id")
    private ExpenseType expenseType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getExpenseCost() {
        return expenseCost;
    }
    
    public void setExpenseCost(Double expenseCost) {
        this.expenseCost = expenseCost;
    }

    public BusinessTravelRequest getBusinessTravelRequest() {
        return businessTravelRequest;
    }

    public void setBusinessTravelRequest(BusinessTravelRequest businessTravelRequest) {
        this.businessTravelRequest = businessTravelRequest;
    }

    public ExpenseType getExpenseType() {
        return expenseType;
    }

    public void setExpenseType(ExpenseType expenseType) {
        this.expenseType = expenseType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Expense expense = (Expense) o;
        if(expense.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, expense.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Expense{" +
            "id=" + id +
            ", expenseCost='" + expenseCost + "'" +
            '}';
    }
}
