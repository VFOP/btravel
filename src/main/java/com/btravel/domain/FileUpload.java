package com.btravel.domain;

import org.hibernate.annotations.*;
import org.hibernate.annotations.Cache;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by FLORINOPREA93 on 4/15/2016.
 */
@Entity
@Table(name = "attachement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "attachement")
public class FileUpload implements Serializable{


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "file_name", nullable = false)
    private String fileName;


    @NotNull
    @Lob
    @Column(name = "file", nullable = false)
    private byte[] file;


    @Column(name = "file_content_type", nullable = false)
    private String fileContentType;

    @ManyToOne
    @JoinColumn(name = "business_travel_request_id")
    private BusinessTravelRequest businessTravelRequest;

    public FileUpload(String fileName, byte[] file, String fileContentType,BusinessTravelRequest businessTravelRequest) {
        this.fileName = fileName;
        this.file = file;
        this.fileContentType = fileContentType;
        this.businessTravelRequest = businessTravelRequest;
    }

    public FileUpload() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }


    public String getFileContentType() {
        return fileContentType;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    public BusinessTravelRequest getBusinessTravelRequest() {
        return businessTravelRequest;
    }

    public void setBusinessTravelRequest(BusinessTravelRequest businessTravelRequest) {
        this.businessTravelRequest = businessTravelRequest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FileUpload fileUpload = (FileUpload) o;
        if(fileUpload.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, fileUpload.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "FileUpload{" +
            "id=" + id +
            ", fileName='" + fileName + "'" +
            ", file='" + file + "'" +
            ", fileContentType='" + fileContentType + "'" +
            '}';
    }
}
