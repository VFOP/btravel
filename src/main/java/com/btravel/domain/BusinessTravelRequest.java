package com.btravel.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import java.time.ZonedDateTime;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.btravel.domain.enumeration.StatusBTR;

/**
 * A BusinessTravelRequest.
 */
@Entity
@Table(name = "business_travel_request")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "businesstravelrequest")
public class BusinessTravelRequest implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private StatusBTR status;

    @Column(name = "start_date")
    private ZonedDateTime startDate;

    @Column(name = "end_date")
    private ZonedDateTime endDate;

    @Column(name = "location")
    private String location;

    @Column(name = "center_cost")
    private String centerCost;

    @Column(name = "request_date")
    private ZonedDateTime requestDate;

    @Column(name = "last_modified_date")
    private ZonedDateTime lastModifiedDate;

    @Column(name = "total_cost")
    private Double totalCost;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "businessTravelRequest")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Expense> expenses = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "assign_to_id")
    private User assignTo;

    @ManyToOne
    @JoinColumn(name = "supplier_id")
    private User supplier;

    @ManyToOne
    @JoinColumn(name = "assign_from_id")
    private User assignFrom;

    @ManyToOne
    @JoinColumn(name = "manager_id")
    private User manager;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StatusBTR getStatus() {
        return status;
    }

    public void setStatus(StatusBTR status) {
        this.status = status;
    }

    public ZonedDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(ZonedDateTime startDate) {
        this.startDate = startDate;
    }

    public ZonedDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(ZonedDateTime endDate) {
        this.endDate = endDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCenterCost() {
        return centerCost;
    }

    public void setCenterCost(String centerCost) {
        this.centerCost = centerCost;
    }

    public ZonedDateTime getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(ZonedDateTime requestDate) {
        this.requestDate = requestDate;
    }

    public ZonedDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(ZonedDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Double totalCost) {
        this.totalCost = totalCost;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Expense> getExpenses() {
        return expenses;
    }

    public void setExpenses(Set<Expense> expenses) {
        this.expenses = expenses;
    }

    public User getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(User user) {
        this.assignTo = user;
    }

    public User getSupplier() {
        return supplier;
    }

    public void setSupplier(User user) {
        this.supplier = user;
    }

    public User getAssignFrom() {
        return assignFrom;
    }

    public void setAssignFrom(User user) {
        this.assignFrom = user;
    }

    public User getManager() {
        return manager;
    }

    public void setManager(User user) {
        this.manager = user;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BusinessTravelRequest businessTravelRequest = (BusinessTravelRequest) o;
        if(businessTravelRequest.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, businessTravelRequest.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "BusinessTravelRequest{" +
            "id=" + id +
            ", status='" + status + "'" +
            ", startDate='" + startDate + "'" +
            ", endDate='" + endDate + "'" +
            ", location='" + location + "'" +
            ", centerCost='" + centerCost + "'" +
            ", requestDate='" + requestDate + "'" +
            ", lastModifiedDate='" + lastModifiedDate + "'" +
            ", totalCost='" + totalCost + "'" +
            '}';
    }
}
