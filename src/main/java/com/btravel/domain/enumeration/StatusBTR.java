package com.btravel.domain.enumeration;

/**
 * The StatusBTR enumeration.
 */
public enum StatusBTR {
    INITIATED,WAITING_FOR_APPROVAL,ISSUING_TICKETS,FINISHED,CLOSED
}
