package com.btravel.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import java.time.ZonedDateTime;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

import com.btravel.domain.enumeration.StatusBTR;
import org.springframework.stereotype.Component;

/**
 * A BusinessTravelRequestHistory.
 */
@Entity
@Component(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Table(name = "business_travel_request_history")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "businesstravelrequesthistory")
public class BusinessTravelRequestHistory implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private StatusBTR status;

    @Column(name = "start_date")
    private ZonedDateTime startDate;

    @Column(name = "end_date")
    private ZonedDateTime endDate;

    @Column(name = "location")
    private String location;

    @Column(name = "center_cost")
    private String centerCost;

    @Column(name = "request_date")
    private ZonedDateTime requestDate;

    @Column(name = "last_modified_date")
    private ZonedDateTime lastModifiedDate;

    @Column(name = "total_cost")
    private Double totalCost;

    @Column(name = "employee")
    private String employee;

    @Column(name = "assign_to")
    private String assignTo;

    @Column(name = "supplier")
    private String supplier;

    @Column(name = "assign_from")
    private String assignFrom;

    @Column(name = "manager")
    private String manager;

    @ManyToOne
    @JoinColumn(name = "business_travel_request_id")
    private BusinessTravelRequest businessTravelRequest;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StatusBTR getStatus() {
        return status;
    }

    public void setStatus(StatusBTR status) {
        this.status = status;
    }

    public ZonedDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(ZonedDateTime startDate) {
        this.startDate = startDate;
    }

    public ZonedDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(ZonedDateTime endDate) {
        this.endDate = endDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCenterCost() {
        return centerCost;
    }

    public void setCenterCost(String centerCost) {
        this.centerCost = centerCost;
    }

    public ZonedDateTime getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(ZonedDateTime requestDate) {
        this.requestDate = requestDate;
    }

    public ZonedDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(ZonedDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Double totalCost) {
        this.totalCost = totalCost;
    }

    public String getEmployee() {
        return employee;
    }

    public void setEmployee(String employee) {
        this.employee = employee;
    }

    public String getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(String assignTo) {
        this.assignTo = assignTo;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getAssignFrom() {
        return assignFrom;
    }

    public void setAssignFrom(String assignFrom) {
        this.assignFrom = assignFrom;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public BusinessTravelRequest getBusinessTravelRequest() {
        return businessTravelRequest;
    }

    public void setBusinessTravelRequest(BusinessTravelRequest businessTravelRequest) {
        this.businessTravelRequest = businessTravelRequest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BusinessTravelRequestHistory businessTravelRequestHistory = (BusinessTravelRequestHistory) o;
        if(businessTravelRequestHistory.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, businessTravelRequestHistory.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "BusinessTravelRequestHistory{" +
            "id=" + id +
            ", status='" + status + "'" +
            ", startDate='" + startDate + "'" +
            ", endDate='" + endDate + "'" +
            ", location='" + location + "'" +
            ", centerCost='" + centerCost + "'" +
            ", requestDate='" + requestDate + "'" +
            ", lastModifiedDate='" + lastModifiedDate + "'" +
            ", totalCost='" + totalCost + "'" +
            ", employee='" + employee + "'" +
            ", assignTo='" + assignTo + "'" +
            ", supplier='" + supplier + "'" +
            ", assignFrom='" + assignFrom + "'" +
            ", manager='" + manager + "'" +
            '}';
    }
}
