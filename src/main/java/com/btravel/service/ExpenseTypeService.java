package com.btravel.service;

import com.btravel.domain.ExpenseType;
import com.btravel.repository.ExpenseTypeRepository;
import com.btravel.repository.search.ExpenseTypeSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ExpenseType.
 */
@Service
@Transactional
public class ExpenseTypeService {

    private final Logger log = LoggerFactory.getLogger(ExpenseTypeService.class);
    
    @Inject
    private ExpenseTypeRepository expenseTypeRepository;
    
    @Inject
    private ExpenseTypeSearchRepository expenseTypeSearchRepository;
    
    /**
     * Save a expenseType.
     * @return the persisted entity
     */
    public ExpenseType save(ExpenseType expenseType) {
        log.debug("Request to save ExpenseType : {}", expenseType);
        ExpenseType result = expenseTypeRepository.save(expenseType);
        expenseTypeSearchRepository.save(result);
        return result;
    }

    /**
     *  get all the expenseTypes.
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<ExpenseType> findAll(Pageable pageable) {
        log.debug("Request to get all ExpenseTypes");
        Page<ExpenseType> result = expenseTypeRepository.findAll(pageable); 
        return result;
    }

    /**
     *  get one expenseType by id.
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public ExpenseType findOne(Long id) {
        log.debug("Request to get ExpenseType : {}", id);
        ExpenseType expenseType = expenseTypeRepository.findOne(id);
        return expenseType;
    }

    /**
     *  delete the  expenseType by id.
     */
    public void delete(Long id) {
        log.debug("Request to delete ExpenseType : {}", id);
        expenseTypeRepository.delete(id);
        expenseTypeSearchRepository.delete(id);
    }

    /**
     * search for the expenseType corresponding
     * to the query.
     */
    @Transactional(readOnly = true) 
    public List<ExpenseType> search(String query) {
        
        log.debug("REST request to search ExpenseTypes for query {}", query);
        return StreamSupport
            .stream(expenseTypeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
