package com.btravel.service.util;

import com.btravel.domain.BusinessTravelRequest;
import com.btravel.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

/**
 * Created by FLORINOPREA93 on 3/28/2016.
 */
public interface Strategy {
    public PageImpl<BusinessTravelRequest> findAll(Page<BusinessTravelRequest> businessTravelRequestsPage,User user);
}
