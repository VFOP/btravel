package com.btravel.service.util;

import com.btravel.domain.BusinessTravelRequest;
import com.btravel.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by FLORINOPREA93 on 3/28/2016.
 */
public class EmployeeBTR implements Strategy{
    @Override
    public PageImpl<BusinessTravelRequest> findAll(Page<BusinessTravelRequest> businessTravelRequestsPage,User user) {

        Iterator<BusinessTravelRequest> businessTravelRequestIterator = businessTravelRequestsPage.iterator();

        List<BusinessTravelRequest> businessTravelRequests = new ArrayList<>();

        while (businessTravelRequestIterator.hasNext()) {

            BusinessTravelRequest businessTravelRequest = businessTravelRequestIterator.next();
            if(businessTravelRequest.getUser().equals(user)) {
                businessTravelRequests.add(businessTravelRequest);
            }
        }

        return new PageImpl<BusinessTravelRequest>(businessTravelRequests);
    }
}
