package com.btravel.service.util;

import com.btravel.domain.BusinessTravelRequest;
import com.btravel.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

/**
 * Created by FLORINOPREA93 on 3/28/2016.
 */
public class Context {
    Strategy strategy;

    public Context(Strategy strategy) {
        this.strategy = strategy;
    }

    public PageImpl<BusinessTravelRequest> executeStrategy(Page<BusinessTravelRequest> businessTravelRequestsPage,User user) {
        return strategy.findAll(businessTravelRequestsPage,user);
    }
}
