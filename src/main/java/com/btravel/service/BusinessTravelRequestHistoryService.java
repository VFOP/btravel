package com.btravel.service;

import com.btravel.domain.BusinessTravelRequestHistory;
import com.btravel.repository.BusinessTravelRequestHistoryRepository;
import com.btravel.repository.search.BusinessTravelRequestHistorySearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing BusinessTravelRequestHistory.
 */
@Service
@Transactional
public class BusinessTravelRequestHistoryService {

    private final Logger log = LoggerFactory.getLogger(BusinessTravelRequestHistoryService.class);

    @Inject
    private BusinessTravelRequestHistoryRepository businessTravelRequestHistoryRepository;

    @Inject
    private BusinessTravelRequestHistorySearchRepository businessTravelRequestHistorySearchRepository;

    /**
     * Save a businessTravelRequestHistory.
     * @return the persisted entity
     */
    public BusinessTravelRequestHistory save(BusinessTravelRequestHistory businessTravelRequestHistory) {
        log.debug("Request to save BusinessTravelRequestHistory : {}", businessTravelRequestHistory);
        BusinessTravelRequestHistory result = businessTravelRequestHistoryRepository.save(businessTravelRequestHistory);
        businessTravelRequestHistorySearchRepository.save(result);
        return result;
    }

    /**
     *  get all the businessTravelRequestHistorys.
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BusinessTravelRequestHistory> findAll(Pageable pageable) {
        log.debug("Request to get all BusinessTravelRequestHistorys");
        Page<BusinessTravelRequestHistory> result = businessTravelRequestHistoryRepository.findAll(pageable);
        return result;
    }

    /**
     *  get one businessTravelRequestHistory by id.
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public BusinessTravelRequestHistory findOne(Long id) {
        log.debug("Request to get BusinessTravelRequestHistory : {}", id);
        BusinessTravelRequestHistory businessTravelRequestHistory = businessTravelRequestHistoryRepository.findOne(id);
        return businessTravelRequestHistory;
    }

    /**
     *  delete the  businessTravelRequestHistory by id.
     */
    public void delete(Long id) {
        log.debug("Request to delete BusinessTravelRequestHistory : {}", id);
        businessTravelRequestHistoryRepository.delete(id);
        businessTravelRequestHistorySearchRepository.delete(id);
    }

    /**
     * search for the businessTravelRequestHistory corresponding
     * to the query.
     */
    @Transactional(readOnly = true)
    public List<BusinessTravelRequestHistory> search(String query) {

        log.debug("REST request to search BusinessTravelRequestHistorys for query {}", query);
        return StreamSupport
            .stream(businessTravelRequestHistorySearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
