package com.btravel.service;

import com.btravel.domain.BusinessTravelRequest;
import com.btravel.domain.FileUpload;
import com.btravel.repository.FileUploadRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by FLORINOPREA93 on 4/15/2016.
 */

@Service
@Transactional
public class FileUploadService {

    private final Logger log = LoggerFactory.getLogger(FileUploadService.class);

    @Inject
    FileUploadRepository fileUploadRepository;

    public FileUpload findByFilename(String filename) {
        return fileUploadRepository.findByFileName(filename);
    }

    public void uploadFile(FileUpload doc) {
        fileUploadRepository.saveAndFlush(doc);
    }

    public List<FileUpload> findAllByBusinessTravelRequest(BusinessTravelRequest businessTravelRequest) {
        return fileUploadRepository.findAllByBusinessTravelRequest(businessTravelRequest);
    }

    public void delete(Long id) {
        log.debug("Request to delete BusinessTravelRequest : {}", id);
        fileUploadRepository.delete(id);
    }

}
