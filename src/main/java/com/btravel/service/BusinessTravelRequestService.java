package com.btravel.service;

import com.btravel.domain.*;
import com.btravel.repository.BusinessTravelRequestRepository;
import com.btravel.repository.ExpenseRepository;
import com.btravel.repository.UserRepository;
import com.btravel.repository.search.BusinessTravelRequestSearchRepository;
import com.btravel.security.SecurityUtils;
import com.btravel.service.util.Context;
import com.btravel.service.util.EmployeeBTR;
import com.btravel.service.util.ManagerBTR;
import com.btravel.service.util.SupplierBTR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Scope;
import java.time.ZonedDateTime;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing BusinessTravelRequest.
 */
@Service
@Transactional
public class BusinessTravelRequestService {

    private final Logger log = LoggerFactory.getLogger(BusinessTravelRequestService.class);

    @Inject
    private BusinessTravelRequestRepository businessTravelRequestRepository;

    @Inject
    private BusinessTravelRequestSearchRepository businessTravelRequestSearchRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    private ExpenseRepository expenseRepository;

    @Inject
    private BusinessTravelRequestHistoryService businessTravelRequestHistoryService;

    /*@Inject
    private BusinessTravelRequestHistory businessTravelRequestHistory;*/

    /**
     * Save a businessTravelRequest.
     * @return the persisted entity
     */

    public Double calculateTotalCost(Long id) {
        log.debug("Method to calculate total cost per business travel request: {}");

        Double totalCost = 0.0;

        BusinessTravelRequest businessTravelRequest = businessTravelRequestRepository.findOne(id);

        List<Expense> expenses = expenseRepository.findAllByBusinessTravelRequest(businessTravelRequest);

        if(expenses.size() != 0) {

            for (Expense expense : expenses) {
                totalCost += expense.getExpenseCost();
            }
            ;

        }

        return totalCost;

    }



    public BusinessTravelRequest save(BusinessTravelRequest businessTravelRequest) {
        log.debug("Request to save BusinessTravelRequest : {}", businessTravelRequest);

        if (businessTravelRequest.getId() != null) {
            businessTravelRequest.setTotalCost(calculateTotalCost(businessTravelRequest.getId()));

        }

        Optional userOptional = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());

        businessTravelRequest.setLastModifiedDate(ZonedDateTime.now());
        businessTravelRequest.setAssignFrom(((User) userOptional.get()));

        BusinessTravelRequestHistory businessTravelRequestHistory = new BusinessTravelRequestHistory();

        businessTravelRequestHistory.setBusinessTravelRequest(businessTravelRequest);
        businessTravelRequestHistory.setStatus(businessTravelRequest.getStatus());
        businessTravelRequestHistory.setStartDate(businessTravelRequest.getStartDate());
        businessTravelRequestHistory.setEndDate(businessTravelRequest.getEndDate());
        businessTravelRequestHistory.setLocation(businessTravelRequest.getLocation());
        businessTravelRequestHistory.setCenterCost(businessTravelRequest.getCenterCost());
        businessTravelRequestHistory.setRequestDate(businessTravelRequest.getRequestDate());
        businessTravelRequestHistory.setLastModifiedDate(businessTravelRequest.getLastModifiedDate());
        businessTravelRequestHistory.setTotalCost(businessTravelRequest.getTotalCost());
        businessTravelRequestHistory.setEmployee(businessTravelRequest.getUser().getLogin());
        businessTravelRequestHistory.setAssignTo(businessTravelRequest.getAssignTo().getLogin());
        businessTravelRequestHistory.setSupplier(businessTravelRequest.getSupplier().getLogin());
        businessTravelRequestHistory.setAssignFrom(businessTravelRequest.getAssignFrom().getLogin());
        businessTravelRequestHistory.setManager(businessTravelRequest.getManager().getLogin());


        businessTravelRequestHistoryService.save(businessTravelRequestHistory);


        BusinessTravelRequest result = businessTravelRequestRepository.save(businessTravelRequest);
        businessTravelRequestSearchRepository.save(result);
        return result;
    }

    /**
     *  get all the businessTravelRequests.
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BusinessTravelRequest> findAll(Pageable pageable) {
        log.debug("Request to get all BusinessTravelRequests");
        Page<BusinessTravelRequest> result = businessTravelRequestRepository.findAll(pageable);

        Optional userOptional = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());

        User user = (User) userOptional.get();

        Iterator<Authority> authorityIterator = user.getAuthorities().iterator();

        while (authorityIterator.hasNext()) {
            switch (authorityIterator.next().getName()) {
                case "ROLE_MANAGER": {
                    Context context = new Context(new ManagerBTR());
                    return context.executeStrategy(result,user);
                }

                case "ROLE_SUPPLIER": {
                    Context context = new Context(new SupplierBTR());
                    return context.executeStrategy(result,user);
                }

                case "ROLE_ADMIN":
                    return result;
            }

            Context context = new Context(new EmployeeBTR());
            return context.executeStrategy(result,user);

        }

        return result;
    }

    /**
     *  get one businessTravelRequest by id.
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public BusinessTravelRequest findOne(Long id) {
        log.debug("Request to get BusinessTravelRequest : {}", id);
        BusinessTravelRequest businessTravelRequest = businessTravelRequestRepository.findOne(id);
        return businessTravelRequest;
    }

    /**
     *  delete the  businessTravelRequest by id.
     */
    public void delete(Long id) {
        log.debug("Request to delete BusinessTravelRequest : {}", id);
        businessTravelRequestRepository.delete(id);
        businessTravelRequestSearchRepository.delete(id);
    }

    /**
     * search for the businessTravelRequest corresponding
     * to the query.
     */
    @Transactional(readOnly = true)
    public List<BusinessTravelRequest> search(String query) {

        log.debug("REST request to search BusinessTravelRequests for query {}", query);
        return StreamSupport
            .stream(businessTravelRequestSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
