package com.btravel.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.btravel.domain.BusinessTravelRequestHistory;
import com.btravel.service.BusinessTravelRequestHistoryService;
import com.btravel.web.rest.util.HeaderUtil;
import com.btravel.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing BusinessTravelRequestHistory.
 */
@RestController
@RequestMapping("/api")
public class BusinessTravelRequestHistoryResource {

    private final Logger log = LoggerFactory.getLogger(BusinessTravelRequestHistoryResource.class);

    @Inject
    private BusinessTravelRequestHistoryService businessTravelRequestHistoryService;

    /**
     * POST  /businessTravelRequestHistorys -> Create a new businessTravelRequestHistory.
     */
    @RequestMapping(value = "/businessTravelRequestHistorys",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<BusinessTravelRequestHistory> createBusinessTravelRequestHistory(@RequestBody BusinessTravelRequestHistory businessTravelRequestHistory) throws URISyntaxException {
        log.debug("REST request to save BusinessTravelRequestHistory : {}", businessTravelRequestHistory);
        if (businessTravelRequestHistory.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("businessTravelRequestHistory", "idexists", "A new businessTravelRequestHistory cannot already have an ID")).body(null);
        }
        BusinessTravelRequestHistory result = businessTravelRequestHistoryService.save(businessTravelRequestHistory);
        return ResponseEntity.created(new URI("/api/businessTravelRequestHistorys/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("businessTravelRequestHistory", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /businessTravelRequestHistorys -> Updates an existing businessTravelRequestHistory.
     */
    @RequestMapping(value = "/businessTravelRequestHistorys",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<BusinessTravelRequestHistory> updateBusinessTravelRequestHistory(@RequestBody BusinessTravelRequestHistory businessTravelRequestHistory) throws URISyntaxException {
        log.debug("REST request to update BusinessTravelRequestHistory : {}", businessTravelRequestHistory);
        if (businessTravelRequestHistory.getId() == null) {
            return createBusinessTravelRequestHistory(businessTravelRequestHistory);
        }
        BusinessTravelRequestHistory result = businessTravelRequestHistoryService.save(businessTravelRequestHistory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("businessTravelRequestHistory", businessTravelRequestHistory.getId().toString()))
            .body(result);
    }

    /**
     * GET  /businessTravelRequestHistorys -> get all the businessTravelRequestHistorys.
     */
    @RequestMapping(value = "/businessTravelRequestHistorys",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<BusinessTravelRequestHistory>> getAllBusinessTravelRequestHistorys(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of BusinessTravelRequestHistorys");
        Page<BusinessTravelRequestHistory> page = businessTravelRequestHistoryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/businessTravelRequestHistorys");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /businessTravelRequestHistorys/:id -> get the "id" businessTravelRequestHistory.
     */
    @RequestMapping(value = "/businessTravelRequestHistorys/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<BusinessTravelRequestHistory> getBusinessTravelRequestHistory(@PathVariable Long id) {
        log.debug("REST request to get BusinessTravelRequestHistory : {}", id);
        BusinessTravelRequestHistory businessTravelRequestHistory = businessTravelRequestHistoryService.findOne(id);
        return Optional.ofNullable(businessTravelRequestHistory)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /businessTravelRequestHistorys/:id -> delete the "id" businessTravelRequestHistory.
     */
    @RequestMapping(value = "/businessTravelRequestHistorys/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteBusinessTravelRequestHistory(@PathVariable Long id) {
        log.debug("REST request to delete BusinessTravelRequestHistory : {}", id);
        businessTravelRequestHistoryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("businessTravelRequestHistory", id.toString())).build();
    }

    /**
     * SEARCH  /_search/businessTravelRequestHistorys/:query -> search for the businessTravelRequestHistory corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/businessTravelRequestHistorys/{query:.+}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<BusinessTravelRequestHistory> searchBusinessTravelRequestHistorys(@PathVariable String query) {
        log.debug("Request to search BusinessTravelRequestHistorys for query {}", query);
        return businessTravelRequestHistoryService.search(query);
    }
}
