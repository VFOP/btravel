package com.btravel.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.btravel.domain.ExpenseType;
import com.btravel.service.ExpenseTypeService;
import com.btravel.web.rest.util.HeaderUtil;
import com.btravel.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ExpenseType.
 */
@RestController
@RequestMapping("/api")
public class ExpenseTypeResource {

    private final Logger log = LoggerFactory.getLogger(ExpenseTypeResource.class);
        
    @Inject
    private ExpenseTypeService expenseTypeService;
    
    /**
     * POST  /expenseTypes -> Create a new expenseType.
     */
    @RequestMapping(value = "/expenseTypes",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ExpenseType> createExpenseType(@RequestBody ExpenseType expenseType) throws URISyntaxException {
        log.debug("REST request to save ExpenseType : {}", expenseType);
        if (expenseType.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("expenseType", "idexists", "A new expenseType cannot already have an ID")).body(null);
        }
        ExpenseType result = expenseTypeService.save(expenseType);
        return ResponseEntity.created(new URI("/api/expenseTypes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("expenseType", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /expenseTypes -> Updates an existing expenseType.
     */
    @RequestMapping(value = "/expenseTypes",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ExpenseType> updateExpenseType(@RequestBody ExpenseType expenseType) throws URISyntaxException {
        log.debug("REST request to update ExpenseType : {}", expenseType);
        if (expenseType.getId() == null) {
            return createExpenseType(expenseType);
        }
        ExpenseType result = expenseTypeService.save(expenseType);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("expenseType", expenseType.getId().toString()))
            .body(result);
    }

    /**
     * GET  /expenseTypes -> get all the expenseTypes.
     */
    @RequestMapping(value = "/expenseTypes",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<ExpenseType>> getAllExpenseTypes(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of ExpenseTypes");
        Page<ExpenseType> page = expenseTypeService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/expenseTypes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /expenseTypes/:id -> get the "id" expenseType.
     */
    @RequestMapping(value = "/expenseTypes/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ExpenseType> getExpenseType(@PathVariable Long id) {
        log.debug("REST request to get ExpenseType : {}", id);
        ExpenseType expenseType = expenseTypeService.findOne(id);
        return Optional.ofNullable(expenseType)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /expenseTypes/:id -> delete the "id" expenseType.
     */
    @RequestMapping(value = "/expenseTypes/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteExpenseType(@PathVariable Long id) {
        log.debug("REST request to delete ExpenseType : {}", id);
        expenseTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("expenseType", id.toString())).build();
    }

    /**
     * SEARCH  /_search/expenseTypes/:query -> search for the expenseType corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/expenseTypes/{query:.+}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ExpenseType> searchExpenseTypes(@PathVariable String query) {
        log.debug("Request to search ExpenseTypes for query {}", query);
        return expenseTypeService.search(query);
    }
}
