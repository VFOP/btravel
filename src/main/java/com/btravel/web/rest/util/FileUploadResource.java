package com.btravel.web.rest.util;

import com.btravel.domain.BusinessTravelRequest;
import com.btravel.domain.FileUpload;
import com.btravel.service.BusinessTravelRequestService;
import com.btravel.service.FileUploadService;
import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.inject.Inject;
import java.util.Iterator;
import java.util.List;

/**
 * Created by FLORINOPREA93 on 4/15/2016.
 */

@CrossOrigin
@RestController
public class FileUploadResource {

    private final Logger log = LoggerFactory.getLogger(FileUploadResource.class);

    @Inject
    BusinessTravelRequestService businessTravelRequestService;

    @Inject
    FileUploadService fileUploadService;

    @RequestMapping(value = "/download", method = RequestMethod.GET)
    public ResponseEntity<?> downloadFile(@RequestParam("filename") String filename) {
        FileUpload fileUpload = fileUploadService.findByFilename(filename);

        if(fileUpload == null) {
            return new ResponseEntity<>("{}", HttpStatus.NOT_FOUND);
        }


        HttpHeaders headers = new HttpHeaders();

        headers.add("content-disposition","attachment; filename="+ fileUpload.getFileName());

        String primaryType,subType;

        try {
            primaryType = fileUpload.getFileContentType().split("/")[0];
            subType = fileUpload.getFileContentType().split("/")[1];
        }catch (IndexOutOfBoundsException | NullPointerException ex) {
            return new ResponseEntity<>("{}",HttpStatus.INTERNAL_SERVER_ERROR);
        }


        headers.setContentType(new MediaType(primaryType,subType));

        return new ResponseEntity<>(fileUpload.getFile(),headers,HttpStatus.OK);

    }


    @RequestMapping(value = "/attachments/{idBtr}", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FileUpload> listAll(@PathVariable Long idBtr ) {
        return fileUploadService.findAllByBusinessTravelRequest(businessTravelRequestService.findOne(Long.valueOf(idBtr)));
    }

    @RequestMapping(value = "/upload",method = RequestMethod.POST)
    public ResponseEntity uploadFile(MultipartHttpServletRequest request) {

        try {
            Iterator<String> iterator = request.getFileNames();

            while (iterator.hasNext()) {
                String uploadFile = iterator.next();
                MultipartFile file = request.getFile(uploadFile);
                String mimeType = file.getContentType();
                String filename = file.getOriginalFilename();

                byte [] bytes = file.getBytes();

                FileUpload newFile = new FileUpload(filename, bytes, mimeType,businessTravelRequestService.findOne(Long.valueOf(1)));

                fileUploadService.uploadFile(newFile);
            }
        }catch (Exception e){
            return new ResponseEntity<>("{}",HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity("{}",HttpStatus.OK);
    }


    @RequestMapping(value = "/attachments/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteAttachment(@PathVariable Long id) {
        log.debug("REST request to delete Attachment : {}", id);
        fileUploadService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("attachment", id.toString())).build();
    }
}
