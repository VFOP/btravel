package com.btravel.web.rest;

import com.btravel.domain.BusinessTravelRequest;
import com.btravel.domain.User;
import com.btravel.domain.enumeration.StatusBTR;
import com.btravel.repository.UserRepository;
import com.btravel.security.SecurityUtils;
import com.btravel.service.BusinessTravelRequestService;
import com.btravel.web.rest.util.HeaderUtil;
import com.btravel.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing BusinessTravelRequest.
 */
@RestController
@RequestMapping("/api")
public class BusinessTravelRequestResource {

    private final Logger log = LoggerFactory.getLogger(BusinessTravelRequestResource.class);

    @Inject
    private BusinessTravelRequestService businessTravelRequestService;

    @Inject
    private UserRepository userRepository;

    /**
     * POST  /businessTravelRequests -> Create a new businessTravelRequest.
     */
    @RequestMapping(value = "/businessTravelRequests",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<BusinessTravelRequest> createBusinessTravelRequest(@RequestBody BusinessTravelRequest businessTravelRequest) throws URISyntaxException {
        log.debug("REST request to save BusinessTravelRequest : {}", businessTravelRequest);
        if (businessTravelRequest.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("businessTravelRequest", "idexists", "A new businessTravelRequest cannot already have an ID")).body(null);
        }

        Optional userOptional = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());

        businessTravelRequest.setStatus(StatusBTR.INITIATED);
        businessTravelRequest.setManager(((User) userOptional.get()));
        businessTravelRequest.setSupplier(businessTravelRequest.getAssignTo());

        BusinessTravelRequest result = businessTravelRequestService.save(businessTravelRequest);
        return ResponseEntity.created(new URI("/api/businessTravelRequests/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("businessTravelRequest", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /businessTravelRequests -> Updates an existing businessTravelRequest.
     */
    @RequestMapping(value = "/businessTravelRequests",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<BusinessTravelRequest> updateBusinessTravelRequest(@RequestBody BusinessTravelRequest businessTravelRequest) throws URISyntaxException {
        log.debug("REST request to update BusinessTravelRequest : {}", businessTravelRequest);
        if (businessTravelRequest.getId() == null) {
            return createBusinessTravelRequest(businessTravelRequest);
        }
        BusinessTravelRequest result = businessTravelRequestService.save(businessTravelRequest);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("businessTravelRequest", businessTravelRequest.getId().toString()))
            .body(result);
    }

    /**
     * GET  /businessTravelRequests -> get all the businessTravelRequests.
     */
    @RequestMapping(value = "/businessTravelRequests",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<BusinessTravelRequest>> getAllBusinessTravelRequests(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of BusinessTravelRequests");
        //businessTravelRequestService.calculateTotalCost();
        Page<BusinessTravelRequest> page = businessTravelRequestService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/businessTravelRequests");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /businessTravelRequests/:id -> get the "id" businessTravelRequest.
     */
    @RequestMapping(value = "/businessTravelRequests/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<BusinessTravelRequest> getBusinessTravelRequest(@PathVariable Long id) {
        log.debug("REST request to get BusinessTravelRequest : {}", id);
        BusinessTravelRequest businessTravelRequest = businessTravelRequestService.findOne(id);
        return Optional.ofNullable(businessTravelRequest)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /businessTravelRequests/:id -> delete the "id" businessTravelRequest.
     */
    @RequestMapping(value = "/businessTravelRequests/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteBusinessTravelRequest(@PathVariable Long id) {
        log.debug("REST request to delete BusinessTravelRequest : {}", id);
        businessTravelRequestService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("businessTravelRequest", id.toString())).build();
    }

    /**
     * SEARCH  /_search/businessTravelRequests/:query -> search for the businessTravelRequest corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/businessTravelRequests/{query:.+}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<BusinessTravelRequest> searchBusinessTravelRequests(@PathVariable String query) {
        log.debug("Request to search BusinessTravelRequests for query {}", query);
        return businessTravelRequestService.search(query);
    }
}
