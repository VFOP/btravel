/**
 * Created by FLORINOPREA93 on 3/9/2016.
 */

'use strict'

angular.module('btravelApp').
    factory('Authority', function($rootScope, $http,$resource){
        return {
          getAllManagers: function(){
              return $http.get('api/users/managers').then(function(response) {
                  return response.data;
              });
          },

          getAllSuppliers: function(){
              return $http.get('api/users/suppliers').then(function(response) {
                  return response.data;
              });
          },

          getLoggedInUser: function(){
              return $http.get('api/users/loggedin').then(function(response) {
                  return response.data;
              });
          },

          getManagerEmployees: function(idManager) {
              return $http.get('api/users/manager/employees/'+idManager).then(function(response) {
                  return response.data;
              });
          },

          getLoginById : function(id) {
              return $http.get('api/users/user/'+id).then(function(response) {
                  return response.data;
              })
          }
        };
    });
