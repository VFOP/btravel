'use strict';

angular.module('btravelApp')
    .factory('BusinessTravelRequestSearch', function ($resource) {
        return $resource('api/_search/businessTravelRequests/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
