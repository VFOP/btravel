'use strict';

angular.module('btravelApp')
    .factory('Expense', function ($resource) {
        var expense = $resource('api/expense/:id', {id: '@id'}, {
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'save': { method: 'POST'},
            'update': { method:'PUT'},
            'delete': { method: 'DELETE',params:{'id':'id'}}
        });

        var expense1 = $resource('api/expenses/:idBtr',{idBtr: '@idBtr'},{
            'query': {method: 'GET', isArray: true}
        });

        var expense2 = $resource('api/expense/',{},{
            'update': {method: 'PUT'}
        });


        return {
            query: function(params,callback) {
                return expense1.query(params,callback);
            },

            get: function(params,callback) {
                return expense.get(params,callback);
            },

            save: function(params,callback) {
                return expense.save(params,callback);
            },

            delete: function(params,callback) {
                return expense.delete(params,callback);
            },

            update: function(params,callback) {
                return expense2.update(params,callback);
            }
        }
    });
