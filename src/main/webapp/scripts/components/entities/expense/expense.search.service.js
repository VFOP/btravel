'use strict';

angular.module('btravelApp')
    .factory('ExpenseSearch', function ($resource) {
        return $resource('api/_search/expense/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
