'use strict';

angular.module('btravelApp')
    .factory('ExpenseTypeSearch', function ($resource) {
        return $resource('api/_search/expenseTypes/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
