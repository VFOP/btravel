'use strict';

angular.module('btravelApp')
    .factory('ExpenseType', function ($resource, DateUtils) {
        return $resource('api/expenseTypes/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
