'use strict';

angular.module('btravelApp')
    .factory('BusinessTravelRequestHistorySearch', function ($resource) {
        return $resource('api/_search/businessTravelRequestHistorys/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
