 'use strict';

angular.module('btravelApp')
    .factory('notificationInterceptor', function ($q, AlertService) {
        return {
            response: function(response) {
                var alertKey = response.headers('X-btravelApp-alert');
                if (angular.isString(alertKey)) {
                    AlertService.success(alertKey, { param : response.headers('X-btravelApp-params')});
                }
                return response;
            }
        };
    });
