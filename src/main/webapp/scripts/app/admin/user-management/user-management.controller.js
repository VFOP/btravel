'use strict';

angular.module('btravelApp')
    .controller('UserManagementController', function ($scope, Principal, User, ParseLinks, Language,Authority) {
        $scope.users = [];
        $scope.authorities = ["ROLE_EMPLOYEE", "ROLE_ADMIN", "ROLE_MANAGER", "ROLE_SUPPLIER"];
        Language.getAll().then(function (languages) {
            $scope.languages = languages;
        });

		Principal.identity().then(function(account) {
            $scope.currentAccount = account;
        });
        $scope.page = 1;
        $scope.loadAll = function () {
            User.query({page: $scope.page - 1, size: 20}, function (result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.users = result;


                var i = 0;

                angular.forEach($scope.users, function(item){
                   Authority.getLoginById(result[i].idManager).then(function(response){
                       item.idManager = response.login;
                   });

                   i++;

                });
            });
        };

        $scope.loadPage = function (page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.setActive = function (user, isActivated) {
            user.activated = isActivated;
            User.update(user, function () {
                $scope.loadAll();
                $scope.clear();
            });
        };

        $scope.clear = function () {
            $scope.user = {
                id: null,idManager:null, login: null, firstName: null, lastName: null, email: null,
                activated: null, langKey: null, createdBy: null, createdDate: null,
                lastModifiedBy: null, lastModifiedDate: null, resetDate: null,
                resetKey: null, authorities: null
            };
            $scope.editForm.$setPristine();
            $scope.editForm.$setUntouched();
        };
    });
