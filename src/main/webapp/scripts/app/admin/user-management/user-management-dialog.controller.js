'use strict';

angular.module('btravelApp').controller('UserManagementDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'User', 'Language','Authority',
        function($scope, $stateParams, $uibModalInstance, entity, User, Language,Authority) {

        $scope.user = entity;
        Authority.getAllManagers().then(function(response){
            $scope.managers = response;
        });


        $scope.authorities = ["ROLE_EMPLOYEE", "ROLE_ADMIN", "ROLE_MANAGER", "ROLE_SUPPLIER"];
        Language.getAll().then(function (languages) {
            $scope.languages = languages;
        });
        var onSaveSuccess = function (result) {
            $scope.isSaving = false;
            $uibModalInstance.close(result);
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.user.id != null) {
                User.update($scope.user, onSaveSuccess, onSaveError);
            } else {
                User.save($scope.user, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
