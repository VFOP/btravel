'use strict';

angular.module('btravelApp')
    .controller('UserManagementDetailController', function ($scope, $stateParams, User,Authority) {
        $scope.user = {};
        $scope.load = function (login) {
            User.get({login: login}, function(result) {
                $scope.user = result;

                Authority.getLoginById(result.idManager).then(function(response){
                    $scope.user.idManager = response.login;

                });
            });
        };
        $scope.load($stateParams.login);
    });
