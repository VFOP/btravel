'use strict';

angular.module('btravelApp').controller('BusinessTravelRequestDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'BusinessTravelRequest', 'User','Authority',
        function($scope, $stateParams, $uibModalInstance, entity, BusinessTravelRequest, User,Authority) {

        $scope.businessTravelRequest = entity;
        Authority.getAllSuppliers().then(function(response){
            $scope.suppliers = response;

        });
        Authority.getLoggedInUser().then(function(response) {
            $scope.loggedIn = response;
          Authority.getManagerEmployees(response.id).then(function(response){
              $scope.employees = response;
            });
        });

        $scope.businessTravelRequest.requestDate = new Date();


        $scope.users = User.query();

        $scope.load = function(id) {
            BusinessTravelRequest.get({id : id}, function(result) {
                $scope.businessTravelRequest = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('btravelApp:businessTravelRequestUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.businessTravelRequest.id != null) {
                BusinessTravelRequest.update($scope.businessTravelRequest, onSaveSuccess, onSaveError);
            } else {
                BusinessTravelRequest.save($scope.businessTravelRequest, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.datePickerForStartDate = {};

        $scope.datePickerForStartDate.status = {
            opened: false
        };

        $scope.datePickerForStartDateOpen = function($event) {
            $scope.datePickerForStartDate.status.opened = true;
        };
        $scope.datePickerForEndDate = {};

        $scope.datePickerForEndDate.status = {
            opened: false
        };

        $scope.datePickerForEndDateOpen = function($event) {
            $scope.datePickerForEndDate.status.opened = true;
        };
        $scope.datePickerForRequestDate = {};

        $scope.datePickerForRequestDate.status = {
            opened: false
        };

        $scope.datePickerForRequestDateOpen = function($event) {
            $scope.datePickerForRequestDate.status.opened = true;
        };
        $scope.datePickerForLastModifiedDate = {};

        $scope.datePickerForLastModifiedDate.status = {
            opened: false
        };

        $scope.datePickerForLastModifiedDateOpen = function($event) {
            $scope.datePickerForLastModifiedDate.status.opened = true;
        };

        $scope.addExpenses = function() {
            $scope.businessTravelRequest.assignTo = $scope.businessTravelRequest.manager;
            $scope.businessTravelRequest.status = 'WAITING_FOR_APPROVAL';
        };

        $scope.finish = function() {
            $scope.businessTravelRequest.status = 'FINISHED';
        }
}]);
