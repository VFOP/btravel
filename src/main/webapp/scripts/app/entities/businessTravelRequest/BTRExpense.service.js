/**
 * Created by FLORINOPREA93 on 5/1/2016.
 */

'use strict'

angular.module('btravelApp').
    factory('BTRExpense',function(){
        var idBtr;

        return {

            setIdBtr : function(id){
                idBtr = id;
            },

            getIdBtr : function() {
                return idBtr;
            }

        };

    });
