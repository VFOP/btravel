'use strict';

angular.module('btravelApp')
    .controller('BusinessTravelRequestDetailController', function ($scope, $rootScope, $stateParams, entity, BusinessTravelRequest, User, Expense) {
        $scope.businessTravelRequest = entity;
        $scope.load = function (id) {
            BusinessTravelRequest.get({id: id}, function(result) {
                $scope.businessTravelRequest = result;
            });
        };
        var unsubscribe = $rootScope.$on('btravelApp:businessTravelRequestUpdate', function(event, result) {
            $scope.businessTravelRequest = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
