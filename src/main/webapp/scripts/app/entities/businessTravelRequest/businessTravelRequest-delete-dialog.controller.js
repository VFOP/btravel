'use strict';

angular.module('btravelApp')
	.controller('BusinessTravelRequestDeleteController', function($scope, $uibModalInstance, entity, BusinessTravelRequest) {

        $scope.businessTravelRequest = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            BusinessTravelRequest.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
