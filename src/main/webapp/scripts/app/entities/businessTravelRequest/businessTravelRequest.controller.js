'use strict';

angular.module('btravelApp')
    .controller('BusinessTravelRequestController', function ($scope, $state, BusinessTravelRequest, BusinessTravelRequestSearch, ParseLinks,Authority) {


        var userLoggedIn;
        var managerN2;

        Authority.getLoggedInUser().then(function(response){

            userLoggedIn = response;
            $scope.loggedIn = response;

            if(userLoggedIn.idManager !== null) {
                Authority.getLoginById(userLoggedIn.idManager).then(function(result){
                    managerN2 = result;
                });
            }
        });

        $scope.businessTravelRequests = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;
        $scope.loadAll = function() {
            BusinessTravelRequest.query({page: $scope.page - 1, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.businessTravelRequests = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            BusinessTravelRequestSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.businessTravelRequests = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.setAccepted = function (id,isAccepted) {

            BusinessTravelRequest.get({id:id}, function (result) {

                $scope.businessTravelRequest = result;
                if (userLoggedIn.idManager !== null) {
                    if (isAccepted) {
                        $scope.businessTravelRequest.status = "WAITING_FOR_APPROVAL";
                        $scope.businessTravelRequest.assignTo = managerN2;
                    } else {
                        $scope.businessTravelRequest.status = "INITIATED";
                        $scope.businessTravelRequest.assignTo = $scope.businessTravelRequest.supplier;
                    }
                }else {

                    if (isAccepted) {
                        $scope.businessTravelRequest.status = "ISSUING_TICKETS";
                        $scope.businessTravelRequest.assignTo = $scope.businessTravelRequest.supplier;
                    } else {
                        $scope.businessTravelRequest.status = "INITIATED";
                        $scope.businessTravelRequest.assignTo = $scope.businessTravelRequest.supplier;
                    }

                }


                BusinessTravelRequest.update($scope.businessTravelRequest,function(){

                    $scope.refresh();

                });
            });
        }
        $scope.clear = function () {
            $scope.businessTravelRequest = {
                status: null,
                startDate: null,
                endDate: null,
                location: null,
                centerCost: null,
                requestDate: null,
                lastModifiedDate: null,
                id: null,
                totalCost: null
            };
        };
    });
