'use strict';

angular.module('btravelApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('businessTravelRequest', {
                parent: 'site',
                url: '/businessTravelRequests',
                data: {
                    authorities: ['ROLE_EMPLOYEE'],
                    pageTitle: 'btravelApp.businessTravelRequest.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/businessTravelRequest/businessTravelRequests.html',
                        controller: 'BusinessTravelRequestController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('businessTravelRequest');
                        $translatePartialLoader.addPart('statusBTR');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('businessTravelRequest.detail', {
                parent: 'site',
                url: '/businessTravelRequest/{id}',
                data: {
                    authorities: ['ROLE_EMPLOYEE'],
                    pageTitle: 'btravelApp.businessTravelRequest.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/businessTravelRequest/businessTravelRequest-detail.html',
                        controller: 'BusinessTravelRequestDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('businessTravelRequest');
                        $translatePartialLoader.addPart('statusBTR');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'BusinessTravelRequest','BTRExpense', function($stateParams, BusinessTravelRequest,BTRExpense) {
                        BTRExpense.setIdBtr($stateParams.id);
                        return BusinessTravelRequest.get({id : $stateParams.id});
                    }]
                }
            })
            .state('businessTravelRequest.new', {
                parent: 'businessTravelRequest',
                url: '/new',
                data: {
                    authorities: ['ROLE_EMPLOYEE'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/businessTravelRequest/businessTravelRequest-dialog.html',
                        controller: 'BusinessTravelRequestDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    status: null,
                                    startDate: null,
                                    endDate: null,
                                    location: null,
                                    centerCost: null,
                                    requestDate: null,
                                    lastModifiedDate: null,
                                    id: null,
                                    totalCost:null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('businessTravelRequest', null, { reload: true });
                    }, function() {
                        $state.go('businessTravelRequest');
                    })
                }]
            })
            .state('businessTravelRequest.edit', {
                parent: 'businessTravelRequest',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_EMPLOYEE'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/businessTravelRequest/businessTravelRequest-dialog.html',
                        controller: 'BusinessTravelRequestDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['BusinessTravelRequest','BTRExpense', function(BusinessTravelRequest,BTRExpense) {
                                BTRExpense.setIdBtr($stateParams.id);
                                return BusinessTravelRequest.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('businessTravelRequest', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('businessTravelRequest.cancel', {
                parent: 'businessTravelRequest',
                url: '/{id}/cancel',
                data: {
                    authorities: ['ROLE_EMPLOYEE'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/businessTravelRequest/businessTravelRequest-cancel-dialog.html',
                        controller: 'BusinessTravelRequestCancelController',
                        size: 'md',
                        resolve: {
                            entity: ['BusinessTravelRequest', function(BusinessTravelRequest) {
                                return BusinessTravelRequest.get({id: $stateParams.id});
                            }]
                        }

                    }).result.then(function(result){
                            $state.go('businessTravelRequest', null,{ reload: true });
                        }, function () {
                            $state.go('^');

                        })
                }]
            })
            .state('businessTravelRequest.delete', {
                parent: 'businessTravelRequest',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_EMPLOYEE'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/businessTravelRequest/businessTravelRequest-delete-dialog.html',
                        controller: 'BusinessTravelRequestDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['BusinessTravelRequest', function(BusinessTravelRequest) {
                                return BusinessTravelRequest.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('businessTravelRequest', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
    });
