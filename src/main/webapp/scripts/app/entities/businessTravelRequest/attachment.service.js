'use strict';

angular.module('btravelApp')
    .factory('Attachment', function ($resource, DateUtils) {
        var attachmentsResource = $resource('attachments/:id', {id: '@id'}, {

            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET', params: {'id': 'id'},
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'delete': {method: 'DELETE',params: {'id': 'id' }}
        });

        var attachmentsResource1 = $resource('attachments/:id', {id: '@id'}, {
            'delete': {method: 'DELETE', params: {'id': 'id'}}
        });

        var attachmentsResource2 = $resource('attachments/:idBtr', {idBtr: '@idBtr'}, {
            'query': {method: 'GET', isArray: true, params: {'idBtr': 'idBtr'}}
        })
        return {
            delete: function(ai,callback) {
                return attachmentsResource1.delete(ai,callback);
            },
            query: function(params,callback) {
                return attachmentsResource2.query(params,callback);
            }
        }
    });





