/**
 * Created by FLORINOPREA93 on 4/15/2016.
 */

'use strict'

angular.module('btravelApp').
    controller('FileUploadController', ['$scope','$interval','Attachment', function($scope,$interval,Attachment) {
        $scope.partialDownloadLink = 'http://localhost:8080/download?filename=';
        $scope.filename = '';

        var btrId;
        $scope.loadAll = function(idBtr) {

            btrId = idBtr;
            Attachment.query({idBtr: idBtr},function(result) {
                $scope.attachments = result;
            });
        };

        $scope.uploadFile = function() {
                $scope.processDropzone();
                debugger;
                $scope.fileUploaded = $scope.file;
            console.log($scope.fileUploaded);
                $scope.loadAll(btrId);

        }

        $scope.deleteFile = function(id) {
            Attachment.delete({'id': id},function(result){
                $scope.loadAll(btrId);
            });
        }

        $scope.reset = function() {
            $scope.resetDropzone();
            $scope.fileUploaded = "";
        }
    }]);
