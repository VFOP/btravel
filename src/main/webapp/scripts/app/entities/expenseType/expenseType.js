'use strict';

angular.module('btravelApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('expenseType', {
                parent: 'admin',
                url: '/expenseTypes',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'btravelApp.expenseType.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/expenseType/expenseTypes.html',
                        controller: 'ExpenseTypeController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('expenseType');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('expenseType.detail', {
                parent: 'admin',
                url: '/expenseType/{id}',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'btravelApp.expenseType.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/expenseType/expenseType-detail.html',
                        controller: 'ExpenseTypeDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('expenseType');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'ExpenseType', function($stateParams, ExpenseType) {
                        return ExpenseType.get({id : $stateParams.id});
                    }]
                }
            })
            .state('expenseType.new', {
                parent: 'expenseType',
                url: '/new',
                data: {
                    authorities: ['ROLE_ADMIN'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/expenseType/expenseType-dialog.html',
                        controller: 'ExpenseTypeDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    type: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('expenseType', null, { reload: true });
                    }, function() {
                        $state.go('expenseType');
                    })
                }]
            })
            .state('expenseType.edit', {
                parent: 'expenseType',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_ADMIN'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/expenseType/expenseType-dialog.html',
                        controller: 'ExpenseTypeDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['ExpenseType', function(ExpenseType) {
                                return ExpenseType.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('expenseType', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('expenseType.delete', {
                parent: 'expenseType',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_ADMIN'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/expenseType/expenseType-delete-dialog.html',
                        controller: 'ExpenseTypeDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['ExpenseType', function(ExpenseType) {
                                return ExpenseType.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('expenseType', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
