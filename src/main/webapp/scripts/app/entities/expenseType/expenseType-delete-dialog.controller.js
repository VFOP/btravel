'use strict';

angular.module('btravelApp')
	.controller('ExpenseTypeDeleteController', function($scope, $uibModalInstance, entity, ExpenseType) {

        $scope.expenseType = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            ExpenseType.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
