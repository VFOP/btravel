'use strict';

angular.module('btravelApp').controller('ExpenseTypeDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'ExpenseType',
        function($scope, $stateParams, $uibModalInstance, entity, ExpenseType) {

        $scope.expenseType = entity;
        $scope.load = function(id) {
            ExpenseType.get({id : id}, function(result) {
                $scope.expenseType = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('btravelApp:expenseTypeUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.expenseType.id != null) {
                ExpenseType.update($scope.expenseType, onSaveSuccess, onSaveError);
            } else {
                ExpenseType.save($scope.expenseType, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
