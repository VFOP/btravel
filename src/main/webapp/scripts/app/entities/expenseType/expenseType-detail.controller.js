'use strict';

angular.module('btravelApp')
    .controller('ExpenseTypeDetailController', function ($scope, $rootScope, $stateParams, entity, ExpenseType, Expense) {
        $scope.expenseType = entity;
        $scope.load = function (id) {
            ExpenseType.get({id: id}, function(result) {
                $scope.expenseType = result;
            });
        };
        var unsubscribe = $rootScope.$on('btravelApp:expenseTypeUpdate', function(event, result) {
            $scope.expenseType = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
