'use strict';

angular.module('btravelApp')
    .controller('ExpenseController', function ($scope, $state, Expense, ExpenseSearch, ParseLinks,Authority,ExpenseType,BTRExpense) {

        var idBTR = BTRExpense.getIdBtr();

        console.log(idBTR);

        Authority.getLoggedInUser().then(function(response) {
            $scope.loggedIn = response;
        });

        $scope.expensetypes = ExpenseType.query();


        $scope.expenses = [];

        $scope.loadAll = function(idBtr) {
            Expense.query({idBtr: idBtr}, function(result) {

                $scope.expenses = result;

                angular.forEach($scope.expenses,function(item){
                    $scope.expensetypes = $scope.expensetypes.filter(function(it){
                        return it.type !== item.expenseType.type;
                    });
                });
            });
        };

        $scope.loadAll(idBTR);


        var onSaveSuccess = function (result) {
            $scope.isSavingExpense = false;
            $scope.expense.expenseType = null;
            $scope.expense.expenseCost =  null;
            $scope.refresh();

        };

        var onSaveError = function (result) {
            $scope.isSavingExpense = false;
        };

        $scope.save = function (businessTravelRequest) {
            $scope.isSavingExpense = true;
            $scope.expense.businessTravelRequest = businessTravelRequest;

            if ($scope.expense.id != null) {
                Expense.update($scope.expense, onSaveSuccess, onSaveError);
            } else {
                Expense.save($scope.expense, onSaveSuccess, onSaveError);
            }
        };

        $scope.delete = function (id) {
            Expense.delete(id, function () {
                $scope.expensetypes = ExpenseType.query();
                $scope.refresh();
            });
        };

        $scope.load = function(id) {
            Expense.get(id, function(result) {
                $scope.expense = result;
            });

        };

        $scope.edit = function (id) {
            $scope.load(id);
        }

        $scope.refresh = function () {
            $scope.loadAll(idBTR);
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.expense = {
                expenseCost: null,
                id: null
            };
        };


    });
