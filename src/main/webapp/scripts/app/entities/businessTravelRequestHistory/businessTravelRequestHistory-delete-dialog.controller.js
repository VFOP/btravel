'use strict';

angular.module('btravelApp')
	.controller('BusinessTravelRequestHistoryDeleteController', function($scope, $uibModalInstance, entity, BusinessTravelRequestHistory) {

        $scope.businessTravelRequestHistory = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            BusinessTravelRequestHistory.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
