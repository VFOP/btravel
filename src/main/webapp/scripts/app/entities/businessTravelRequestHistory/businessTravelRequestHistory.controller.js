'use strict';

angular.module('btravelApp')
    .controller('BusinessTravelRequestHistoryController', function ($scope, $state, BusinessTravelRequestHistory, BusinessTravelRequestHistorySearch, ParseLinks) {

        $scope.businessTravelRequestHistorys = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;
        $scope.loadAll = function() {
            BusinessTravelRequestHistory.query({page: $scope.page - 1, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.businessTravelRequestHistorys = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            BusinessTravelRequestHistorySearch.query({query: $scope.searchQuery}, function(result) {
                $scope.businessTravelRequestHistorys = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.businessTravelRequestHistory = {
                status: null,
                startDate: null,
                endDate: null,
                location: null,
                centerCost: null,
                requestDate: null,
                lastModifiedDate: null,
                totalCost: null,
                employee: null,
                assignTo: null,
                supplier: null,
                assignFrom: null,
                manager: null,
                id: null
            };
        };
    });
