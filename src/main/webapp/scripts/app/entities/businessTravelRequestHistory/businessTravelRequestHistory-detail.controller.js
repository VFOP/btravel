'use strict';

angular.module('btravelApp')
    .controller('BusinessTravelRequestHistoryDetailController', function ($scope, $rootScope, $stateParams, entity, BusinessTravelRequestHistory, BusinessTravelRequest) {
        $scope.businessTravelRequestHistory = entity;
        $scope.load = function (id) {
            BusinessTravelRequestHistory.get({id: id}, function(result) {
                $scope.businessTravelRequestHistory = result;
            });
        };
        var unsubscribe = $rootScope.$on('btravelApp:businessTravelRequestHistoryUpdate', function(event, result) {
            $scope.businessTravelRequestHistory = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
