'use strict';

angular.module('btravelApp').controller('BusinessTravelRequestHistoryDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'BusinessTravelRequestHistory', 'BusinessTravelRequest',
        function($scope, $stateParams, $uibModalInstance, entity, BusinessTravelRequestHistory, BusinessTravelRequest) {

        $scope.businessTravelRequestHistory = entity;
        $scope.businesstravelrequests = BusinessTravelRequest.query();
        $scope.load = function(id) {
            BusinessTravelRequestHistory.get({id : id}, function(result) {
                $scope.businessTravelRequestHistory = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('btravelApp:businessTravelRequestHistoryUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.businessTravelRequestHistory.id != null) {
                BusinessTravelRequestHistory.update($scope.businessTravelRequestHistory, onSaveSuccess, onSaveError);
            } else {
                BusinessTravelRequestHistory.save($scope.businessTravelRequestHistory, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.datePickerForStartDate = {};

        $scope.datePickerForStartDate.status = {
            opened: false
        };

        $scope.datePickerForStartDateOpen = function($event) {
            $scope.datePickerForStartDate.status.opened = true;
        };
        $scope.datePickerForEndDate = {};

        $scope.datePickerForEndDate.status = {
            opened: false
        };

        $scope.datePickerForEndDateOpen = function($event) {
            $scope.datePickerForEndDate.status.opened = true;
        };
        $scope.datePickerForRequestDate = {};

        $scope.datePickerForRequestDate.status = {
            opened: false
        };

        $scope.datePickerForRequestDateOpen = function($event) {
            $scope.datePickerForRequestDate.status.opened = true;
        };
        $scope.datePickerForLastModifiedDate = {};

        $scope.datePickerForLastModifiedDate.status = {
            opened: false
        };

        $scope.datePickerForLastModifiedDateOpen = function($event) {
            $scope.datePickerForLastModifiedDate.status.opened = true;
        };
}]);
