'use strict';

angular.module('btravelApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('businessTravelRequestHistory', {
                parent: 'admin',
                url: '/businessTravelRequestHistorys',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'btravelApp.businessTravelRequestHistory.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/businessTravelRequestHistory/businessTravelRequestHistorys.html',
                        controller: 'BusinessTravelRequestHistoryController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('businessTravelRequestHistory');
                        $translatePartialLoader.addPart('statusBTR');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('businessTravelRequestHistory.detail', {
                parent: 'admin',
                url: '/businessTravelRequestHistory/{id}',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'btravelApp.businessTravelRequestHistory.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/businessTravelRequestHistory/businessTravelRequestHistory-detail.html',
                        controller: 'BusinessTravelRequestHistoryDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('businessTravelRequestHistory');
                        $translatePartialLoader.addPart('statusBTR');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'BusinessTravelRequestHistory', function($stateParams, BusinessTravelRequestHistory) {
                        return BusinessTravelRequestHistory.get({id : $stateParams.id});
                    }]
                }
            })
            .state('businessTravelRequestHistory.new', {
                parent: 'businessTravelRequestHistory',
                url: '/new',
                data: {
                    authorities: ['ROLE_ADMIN'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/businessTravelRequestHistory/businessTravelRequestHistory-dialog.html',
                        controller: 'BusinessTravelRequestHistoryDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    status: null,
                                    startDate: null,
                                    endDate: null,
                                    location: null,
                                    centerCost: null,
                                    requestDate: null,
                                    lastModifiedDate: null,
                                    totalCost: null,
                                    employee: null,
                                    assignTo: null,
                                    supplier: null,
                                    assignFrom: null,
                                    manager: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('businessTravelRequestHistory', null, { reload: true });
                    }, function() {
                        $state.go('businessTravelRequestHistory');
                    })
                }]
            })
            .state('businessTravelRequestHistory.edit', {
                parent: 'businessTravelRequestHistory',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_ADMIN'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/businessTravelRequestHistory/businessTravelRequestHistory-dialog.html',
                        controller: 'BusinessTravelRequestHistoryDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['BusinessTravelRequestHistory', function(BusinessTravelRequestHistory) {
                                return BusinessTravelRequestHistory.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('businessTravelRequestHistory', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('businessTravelRequestHistory.delete', {
                parent: 'businessTravelRequestHistory',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_ADMIN'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/businessTravelRequestHistory/businessTravelRequestHistory-delete-dialog.html',
                        controller: 'BusinessTravelRequestHistoryDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['BusinessTravelRequestHistory', function(BusinessTravelRequestHistory) {
                                return BusinessTravelRequestHistory.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('businessTravelRequestHistory', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
